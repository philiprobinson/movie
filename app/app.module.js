'use strict';

angular.module('movieApp', [
    'backand',
    /*'ngResource',*/
    /*'ngSanitize',*/
    'ui.router',
    /*'ui.sortable',*/
    'movieApp.config.interceptors',
    'movieApp.config.consts',
    'uiRouterStyles'
])
    .config(['$stateProvider', '$httpProvider', '$urlRouterProvider', 'BackandProvider', 'CONSTS',
        function ($stateProvider, $httpProvider, $urlRouterProvider, BackandProvider, CONSTS) {
            BackandProvider.setAnonymousToken(CONSTS.anonymousToken)
                .setSignUpToken(CONSTS.signUpToken)
                .setAppName(CONSTS.appName);

            //By default in the SDK when signup is success it's automatically signin.
            //In this app we wanted to show all the process so we turned it off.
            BackandProvider.runSigninAfterSignup(false);

            $httpProvider.interceptors.push('todoHttpInterceptor');

           $urlRouterProvider.otherwise("home");

            $stateProvider
                .state('main', {
                    url: '/',
                    abstract: true,
                    templateUrl: 'app/components/header/header.html',
                    controller: 'headerCtrl as header'
                })
                .state('home', {
                    url: 'home',
                    parent: 'main',
                    templateUrl: 'app/components/home/home.html',
                    controller: 'homeCtrl'
                })
                .state('comics', {
                    url: 'comics',
                    parent: 'main',
                    templateUrl: 'app/components/comics/comics.html',
                    controller: 'comicsCtrl'
                })
                .state('animated', {
                    url: 'animated',
                    parent: 'main',
                    templateUrl: 'app/components/animated/animated.html',
                    controller: 'animatedCtrl'
                })
                .state('search', {
                    url: 'search',
                    parent: 'main',
                    templateUrl: 'app/components/search/search.html',
                    controller: 'searchCtrl'
                })
                .state('login', {
                    url: 'login',
                    parent: 'main',
                    templateUrl: 'app/components/login/login.html',
                    controller: 'loginCtrl as login'
                })
                .state('profile', {
                    url: 'profile',
                    parent: 'main',
                    templateUrl: 'app/components/profile/profile.html',
                    controller: 'profileCtrl as profile'
                })            
                .state('cronjob', {
                    url: '/cronjob',
                    templateUrl: 'app/components/cronjob/cronjob.html',
                    controller: 'cronCtrl as cronjob'
                });             

        }]);