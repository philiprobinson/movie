/*(function () {

angular.module("movieApp")
	.config(function ($routeProvider, $stateProvider, $urlRouterProvider) {
    
    // For any unmatched url, redirect to /state1
  $urlRouterProvider.otherwise("/home");
    
		$stateProvider

		// route for the home page
        .state('header', {
            url: '/',
			templateUrl: 'app/components/header/header.html',
			controller: 'headerCtrl',
            abstract: true
		})        
        .state('home', {
            url: '/home',
			templateUrl: 'app/components/home/home.html',
			controller: 'homeCtrl',
            parent: 'header'
		})
        .state('comics', {
            url: '/comics',
			templateUrl: 'app/components/comics/comics.html',
			controller: 'comicsCtrl',
            parent: 'header'
		})
        .state('animated', {
            url: '/animated',
			templateUrl: 'app/components/animated/animated.html',
			controller: 'animatedCtrl'
		})
        .state('search', {
            url: '/search',
			templateUrl: 'app/components/search/search.html',
			controller: 'searchCtrl'
		})
        .state('login', {
            url: '/login',
			templateUrl: 'app/components/login/login.html',
			controller: 'loginCtrl',
            controllerAs: 'login'
		});
	});

})();*/