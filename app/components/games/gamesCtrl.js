(function () {
    var gamesCtrl = function ($scope, gamesFactory) {
        
        var promise = gamesFactory.getGames();
        
        promise.then(function (result) {
            $scope.games = result.results;
        });
    };

    angular.module("movieApp")
        .controller("gamesCtrl", gamesCtrl);
})();
