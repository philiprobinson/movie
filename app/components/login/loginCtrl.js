(function () {
    var loginCtrl = function (AuthService, $state, Backand) {

        var self = this;

        if (AuthService.currentUser.username != " ") {
            self.login = "Log out";
        } else {
            self.login = "Log in";
        }

        self.logout = function () {
            AuthService.logout();
            window.location.assign('#/login');
            console.log("logged out");
            self.login = "Log in";

        };
        //console.log(AuthService.currentUser);
        //console.log(Backand.getUserDetails().$$state.value.firstName);

        self.appName = AuthService.appName;
        self.error = $state.params.error;

        self.authenticate = function () {
            self.error = null;
            self.success = null;

            if (self.newUser) {
                self.signup();
            } else {
                self.signin();
            }
        };

        self.signup = function () {
            var parameters = {
                company: self.company || ''
            };

            AuthService.signup(self.firstName, self.lastName, self.username, self.password, parameters)
                .then(
                    function (response) {
                        //check status of the sign in
                        switch (response.data.currentStatus) {
                        case 1: // The user is ready to sign in
                            window.location.assign('#/');
                            break;
                        case 2: //The system is now waiting for the user to respond a verification email.
                            self.success = 'Please check your email to continue';
                            break;
                        case 3: //The user signed up and is now waiting for an administrator approval.
                            self.success = 'Please wait for the administrator to approve the sign up';
                            break;
                        }
                    }, showError
                );
        };

        self.signin = function () {
            AuthService.signin(self.username, self.password)
                .then(
                    function () {
                        window.location.assign('#/');
                        self.login = "Log out";
                        window.location.reload();
                        console.log(AuthService.currentUser);
                    },
                    function (data) {
                        showError(data);
                    }
                );
        };

        function showError(error) {
            console.log(error);
            if (error.error_description == "The user name or password is incorrect.") {
                self.error = error.error_description;
            }
            if (error.data.error_description == "The user is already signed up to this app") {
                self.error = error.data.error_description;
            } else {
                self.error = error && error.data || 'Unknown error from server';
            }
        }

    };

    angular.module("movieApp")
        .controller('loginCtrl', ['AuthService', '$state', loginCtrl]);
})();