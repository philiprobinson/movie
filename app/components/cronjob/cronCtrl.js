(function () {
    var cronCtrl = function (trackService) {
        
        function init() {
            checkMovieDates();
        }
        init();
        
        function checkMovieDates () {
            trackService.checkMovieDates();
            console.log("checking movie dates..");
        }  
        
    };

    angular.module("movieApp")
        .controller("cronCtrl", cronCtrl);
})();