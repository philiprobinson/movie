(function () {
    var searchCtrl = function ($scope, searchFactory, $timeout, $http, $q) {

        $timeout(function () {
            $scope.sLogo = true;
        });
        
        $(document).on("keyup", "#search", function (e) {
            if (e.keyCode == 13) {
                window.location.replace('#/search');
                $scope.searchMovies();
            }
        });
        
        $scope.search_movieData = []; // youtube/imdb-ids
        $scope.search_movieIDs = []; // Movie ids
        $scope.search = document.getElementById("search").value;
        var searchID = document.getElementById("search").value;

        var deferred = $q.defer();
        var promise = $q.all(null);

        // /questions/1129216/sort-array-of-objects-by-string-property-value-in-javascript
        function dynamicSort(property) {
            var sortOrder = -1;
            if (property[0] === "-") {
                sortOrder = 1;
                property = property.substr(1);
            }
            return function (a, b) {
                var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
                return result * sortOrder;
            };
        }

        $http({
            method: "GET",
            url: "http://api.themoviedb.org/3/search/movie?api_key=626fa111e42b123ed4a234187d5b45d4&query=" + searchID + '"',
        }).success(function (result) {
            //deferred.resolve(result);
            result.results.sort(dynamicSort("release_date")); // Sort result by release date                
            for (var x = 0; x < result.results.length; x++) {
                $scope.search_movieIDs.push(result.results[x].id);
            }
            addMoviIds();
        }/*, function errorCallback(response) {
            alert("Movie requests exceeded, wait a few seconds and try again.");
        }*/);

        function addMoviIds() {
            angular.forEach($scope.search_movieIDs, function (movieIDs) {
                promise = promise.then(function () { // Adding queue?
                    return $http({
                        method: 'GET',
                        url: "http://api.themoviedb.org/3/movie/" + movieIDs + "?api_key=626fa111e42b123ed4a234187d5b45d4&append_to_response=videos"
                    }).then(function (response) {
                        $scope.search_movieData.push(response.data);
                    }/*, function errorCallback(response) {
                        alert("Movie requests exceeded, wait a few seconds and try again.");
                    }*/);
                });
            });
        }

        $scope.searchMovies = function () {
            var searchID = document.getElementById("search").value;
            $scope.search_movieIDs = [];
            $scope.search_movieData = [];
            $http({
                method: "GET",
                url: "http://api.themoviedb.org/3/search/movie?api_key=626fa111e42b123ed4a234187d5b45d4&query=" + searchID + '"',
            }).success(function (result) {
                //deferred.resolve(result);
                result.results.sort(dynamicSort("release_date")); // Sort result by release date                
                for (var x = 0; x < result.results.length; x++) {
                    $scope.search_movieIDs.push(result.results[x].id);
                }
                addMoviIds();
            }/*, function errorCallback(response) {
                alert("Movie requests exceeded, wait a few seconds and try again.");
            }*/);
        };

    };

    angular.module("movieApp")
        .controller("searchCtrl", searchCtrl);
})();