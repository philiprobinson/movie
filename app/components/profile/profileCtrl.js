(function () {
    var profileCtrl = function (AuthService, $state, Backand, trackService) {

        var self = this;

        // list users tracked movies in profile page
        self.profileTracks = function () {
            var userName = Backand.getUserDetails().$$state.value.username;

            trackService.profileTracks(userName).then(function (response) {
                self.trackedMovies = response.data.data;
            });
        };
        self.profileTracks();

        // remove tracked movie by clicking icon in profile
        $("body").on("click", ".untrackMovie", function () {
            self.movieTitle = $(this).prevAll("li").text().substring(12).trim();
            self.userName = Backand.getUserDetails().$$state.value.username;

            for(var i = 0; i < self.trackedMovies.length; i++) {
                if (self.trackedMovies[i].title === self.movieTitle) {
                    self.trackedMovies.splice(i, 1);
                   
                }
            }

            console.log("Untracking" + " " + $(this).prevAll("li").text().trim());
            trackService.untrackMovie(self.movieTitle, self.userName);

        });

    };

    angular.module("movieApp")
        .controller('profileCtrl', profileCtrl);
})();