(function () {

    var headerCtrl = function ($state, AuthService, Backand, $http) {
        var self = this;

        self.currentUserMail = AuthService.currentUser;
        //self.userFirstName = Backand.getUserDetails().$$state.value.firstName;
        //self.userFullName = Backand.getUserDetails().$$state.value.fullName;

        /**
         * Logout from Backand
         */
        self.logout = function () {
            AuthService.logout();
            //$state.go('login');
            window.location.assign('#/login');
            window.location.reload();
        };

        self.isloggedIn = function() {
            if (Backand.getUserDetails().$$state.value !== null) {
            return true;
        } else {
            return false;
        }
        };
        
        if (Backand.getUserDetails().$$state.value !== null) {
            self.login = "Log out";
        } else {
            self.login = "Log in";
        }

        self.toggleMenu = function () {
            // If menu is already showing, slide it up. Otherwise, slide it down.
            $('.menuContainer').toggleClass('slide-down');

            $('.menuContainer a').on("click", function () {
                $('.menuContainer').removeClass('slide-down');
            });
        };

    };
    angular.module('movieApp')
        .controller('headerCtrl', headerCtrl);

})();