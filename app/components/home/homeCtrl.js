(function () {
    var homeCtrl = function ($scope, moviesFactory, Backand, trackService, AuthService) {

        var rel_date; // release date for tracked movie
        var title; // title of movie
        var trackedMovies; // Object of current users tacked movies
        var trackedMoviesLength; // trackedMovies object/array length
        
        $scope.movieData = moviesFactory.getMovieData(); // Youtube/imdb-ids
        var promise = moviesFactory.getMovies(); // Movie object
        
        
        // init list of tracked movies to add checkmarks
        function init() {
            if (Backand.getUserDetails().$$state.value !== null) {
                var userName = Backand.getUserDetails().$$state.value.username;
                
                trackService.getMovieObj(userName).then(function(response) {
                    trackedMovies = response.data.data; // tracked movies
                    trackedMoviesLength = trackedMovies.length; // object length
                });
            }
        }
        init();

        // final movie object/$scope
        promise.then(function (result) {
            $scope.movies = result.results;
        });
        
        // Add track icon if user logged in
        $scope.isUserLoggedIn = function () {
            if (Backand.getUserDetails().$$state.value !== null) {
                return true;
            } else {
                return false;
            }
        };
        
        // check which movies current user is tracking
        $scope.IsTracking = function (movieTitle) {
            for(var i = 0; i < trackedMoviesLength; i++) {
                if(movieTitle === trackedMovies[i].title) {
                    return true;
                }
            }
        };

        // Tracking movie on click
        $("body").off().on("click", ".trackMovie", function() {
            // User details
            var userId = Backand.getUserDetails().$$state.value.userId;
            var userName = Backand.getUserDetails().$$state.value.username;
            
            rel_date = $(this).prevAll(".rel_date").text();
            title = $(this).prevAll(".title").text();
            console.log("Tracking: " + " " + rel_date + " " + title);
            
            trackService.addMovie(title, rel_date, userName, userId);
            $(this).attr('src','images/checkIcon.png');
            $(this).removeClass('trackMovie');
        });

        // Untrack movie
        $scope.untrack = function (e) {
            var movieTitle = $(e.target).prevAll(".title").text();
            var user = AuthService.currentUser.name;
            
            console.log("Untracking" + " " + $(e.target).prevAll(".title").text());
            trackService.untrackMovie(movieTitle, user);  
        };
        
        
        $(document).on("keyup", "#search", function (e) {
            if (e.keyCode == 13) {
                window.location.replace('#/search');
            }
        });
    };

    angular.module("movieApp")
        .controller("homeCtrl", homeCtrl);
})();