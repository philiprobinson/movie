angular.module("movieApp").directive('fadeIn', function () {

    jQuery.extend(jQuery.easing, {
        easeOutExpo: function (x, t, b, c, d) {
            return (t == d) ? b + c : c * (-Math.pow(2, -10 * t / d) + 1) + b;
        }
    });
    
    return function (scope, element, attrs) {
        setTimeout(function () {
            angular.element(".posterContainer").each(function (i) {
                angular.element(this).delay(i * 100).animate({
                    'opacity': '1'
                }, 2000, 'easeOutExpo');
            });
        }, 0);
    };

});