angular.module("movieApp").directive('checkImage', function() {
   return {
      link: function(scope, element, attrs) {
         element.on('error', function() {
            element.attr('src', 'images/noPoster.png'); // set default image
         });
       }
   };
});