(function() {
  angular.module('movieApp.config.consts', [])
    .constant('CONSTS', {
      anonymousToken: '0a4d7f83-0b73-4bfb-b7bf-dabd01c983b4',
      signUpToken: '4d124826-2be6-4898-8d5d-bef52afef866',
      appName: 'moviesapp'
    });

})();
