(function () {

    var trackService = function ($http, Backand) {

        var currentDate = new Date(); // used for check date function
        
        // Tracked movies listed in users profile page
        var profileTracks = function (user) {
            return $http({
                method: 'GET',
                url: "https://api.backand.com:443/1/objects/movie",
                params: {
                  filter: {
                    "q": {
                        "$or": [{ "checked": true, "email" : { "$eq" : user }}]
                    }
                  }
                },
                headers: {
                    'Content-Type': 'application/json'
                },
            });
        };

        // movie object used to add check marks on pages
        var getMovieObj = function (user) {
            return $http({
                method: 'GET',
                url: 'https://api.backand.com/1/objects/movie?pageSize=20&pageNumber=1&filter=%5B%7B%22fieldName%22%3A%22email%22%2C%22operator%22%3A%22equals%22%2C%22value%22%3A%22' + user + '%22%7D%5D',
                params: {
                    pageSize: 20,
                    pageNumber: 1,
                    filter: null,
                    sort: ''
                }
            });
        };
        
        // Post function to add movie to track list
        var addMovie = function (title, rel_date, email, userId) {
            return $http({
                method: 'POST',
                url: "https://api.backand.com/1/objects/movie",
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    title: title,
                    date: rel_date,
                    email: email,
                    added_by: userId,
                    checked: true
                }
            }).success(function () {
                console.log("SENT!");
            });
        };

        // Delete tracked movie
        var untrackMovie = function (movieTitle, user) {
            return $http({
                method: 'GET',
                url: "https://api.backand.com/1/objects/movie",
                params: {
                  filter: {
                    "q": {
                        "$or": [{ "title": movieTitle, "email" : { "$eq" : user }}]
                    }
                  }
                },
                headers: {
                    'Content-Type': 'application/json'
                },

            }).then(function (response) {
               return $http({
                    method: 'DELETE',
                    url: "https://api.backand.com/1/objects/movie/" + response.data.data[0].id
                });
            });
        };

        // Check dates of movies before sending mails
        var checkMovieDates = function () {
            return $http({
                method: 'GET',
                url: 'https://api.backand.com/1/objects/movie',
                params: {
                    pageSize: 20,
                    pageNumber: 1,
                    filter: null,
                    sort: ''
                }
            }).then(function (response) {
                for (var i = 0; i < response.data.data.length; i++) {
                    var tmpDate = new Date(response.data.data[i].date);
                    var difDate = (tmpDate - currentDate) / (1000 * 3600 * 24);

                    // Check date condition (between todays date and 2 weeks forward/back)
                    if ((difDate >= 0) && (difDate <= 15) && response.data.data[i].checked === true ||                           (difDate < 0) && (difDate >= -15) && response.data.data[i].checked === true) {
                        console.log("Hit!" + " " + response.data.data[i].title + " - " +
                            response.data.data[i].date.substring(0, 10) + " " + "id" +
                            " " + response.data.data[i].id + " " + response.data.data[i].email);

                        sendMail(response.data.data[i].email,
                            response.data.data[i].date.substring(0, 10),
                            response.data.data[i].title,
                            response.data.data[i].id);

                    } else {
                        console.log("No hit");
                    }
                }
            });
        };

        // Send mails
        var sendMail = function (email, date, title, id) {
            return $http({
                method: 'GET',
                url: 'https://api.backand.com/1/objects/action/movie/' + id + '?name=mandrillapp',
                params: {
                    parameters: {
                        message: 'Movie:' + " " + title + " " + "arriving" + " " + date,
                        name: 'MovieApp',
                        email: email
                    }
                }
            }).then(function () {
                console.log("Mail sent to " + " " + email);
                checkSent(id);
            });
        };

        // Checked attribute to false after mail have been sent, to prevent duplicates
        var checkSent = function (id) {
            return $http({
                method: 'PUT',
                url: 'https://api.backand.com/1/objects/movie/' + id,
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    checked: false
                }
            }).then(function () {
                console.log("Checked to false");
            });
        };

        return {
            addMovie: addMovie,
            sendMail: sendMail,
            checkMovieDates: checkMovieDates,
            untrackMovie: untrackMovie,
            getMovieObj: getMovieObj,
            profileTracks: profileTracks

        };

    };

    angular.module("movieApp").factory("trackService", trackService);

})();