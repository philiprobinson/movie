(function () {

    var gamesFactory = function ($http, $q) {

        var factory = {}; // Factory object

        var deferred = $q.defer();

        $http({
            method: "GET",
            url: "https://www.igdb.com/api/v1/games?token=gvPfTrgoUCAfZWUZH_T1O0ybsLphVDW-Fv8THKekcso",
            dataType: 'json',
            headers: {
                'Content-type': 'application/javascript',
            },
        }).success(function (result) {
            console.log(result);
            deferred.resolve(result);
        });

        factory.getGames = function () {
            return deferred.promise;
        };

        return factory;

    };

    angular.module("movieApp").factory("gamesFactory", gamesFactory);

})();