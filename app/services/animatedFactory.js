(function () {

    var animatedFactory = function ($http, $q) {

        var factory = {}; // Factory object
        var movieData = []; // youtube/imdb-ids
        var movieIDs = []; // Movie ids

        var deferred = $q.defer();
        var promise = $q.all(null);

        // Get current date and save in var "today" 
        // Add one year in var "inAyear"
        var today = new Date();
        var inAyear = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear();

        if(dd<10) {dd='0'+dd;} 
        if(mm<10) {mm='0'+mm;} 
        today = yyyy+'-'+mm+'-'+dd;
        inAyear = yyyy+1 +'-'+mm+'-'+dd;

        
        // /questions/1129216/sort-array-of-objects-by-string-property-value-in-javascript
        function dynamicSort(property) {
            var sortOrder = 1;
            if (property[0] === "-") {
                sortOrder = -1;
                property = property.substr(1);
            }
            return function (a, b) {
                var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
                return result * sortOrder;
            };
        }

        $http({
            method: "GET",
            url: "https://api.themoviedb.org/3/discover/movie?api_key=626fa111e42b123ed4a234187d5b45d4&primary_release_date.gte=" + today + "&primary_release_date.lte=" + inAyear + "&with_genres=16&vote_count.gte=1",
        }).success(function (result) {
            deferred.resolve(result);
            result.results.sort(dynamicSort("release_date")); // Sort result by release date
            for (var x = 0; x < result.results.length; x++) {
                movieIDs.push(result.results[x].id);
            }
            addMoviIds();
        });

        function addMoviIds() {
            angular.forEach(movieIDs, function (movieIDs) {
                promise = promise.then(function () { // Adding queue?
                    return $http({
                        method: 'GET',
                        url: "http://api.themoviedb.org/3/movie/" + movieIDs + "?api_key=626fa111e42b123ed4a234187d5b45d4&append_to_response=videos"
                    }).then(function (response) {
                        movieData.push(response.data);
                    });
                });
            });
        }

        factory.getMovies = function () {
            return deferred.promise;
        };
        factory.getMovieData = function () {
            return movieData;
        };

        return factory;

    };

    angular.module("movieApp").factory("animatedFactory", animatedFactory);

})();