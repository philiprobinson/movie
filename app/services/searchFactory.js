(function () {

    var searchFactory = function ($http, $q) {

        var factory = {}; // Factory object
        var movieData = []; // youtube/imdb-ids
        var movieIDs = []; // Movie ids
        //var searchID = "warcraft";

        var deferred = $q.defer();
        var promise = $q.all(null);


        factory.searchMovies = function(searchID) {
            $http({
                method: "GET",
                url: "http://api.themoviedb.org/3/search/movie?api_key=626fa111e42b123ed4a234187d5b45d4&query=" + searchID + '"',
            }).success(function (result) {
                deferred.resolve(result);
                for (var x = 0; x < result.results.length; x++) {
                    movieIDs.push(result.results[x].id);
                }
                addMoviIds();
            });
        };

        function addMoviIds() {
            angular.forEach(movieIDs, function (movieIDs) {
                promise = promise.then(function () { // Adding queue?
                    return $http({
                        method: 'GET',
                        url: "http://api.themoviedb.org/3/movie/" + movieIDs + "?api_key=626fa111e42b123ed4a234187d5b45d4&append_to_response=videos"
                    }).then(function (response) {
                        movieData.push(response.data);
                    });
                });
            });
        }

        factory.getMovies = function () {
            return deferred.promise;
        };
        factory.getMovieData = function () {
            return movieData;
        };

        return factory;

    };

    angular.module("movieApp").factory("searchFactory", searchFactory);

})();