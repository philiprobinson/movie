# Beskrivning #
Tanken med denna applikation är att man enkelt kan få en överblick på vilka filmer som snart har premiär. Informationen om filmerna hämtas från www.themoviedb.org. http://backand.com/ Används för backend. 

**Update:** Det går nu även att registrera/logga in, som inloggad kan du följa kommande filmer. 
Appen skickar ut en påminnelse via mail två veckor innan filmer som du följer har premiär.

# Installation #

Hämta filerna och kör index.html. Sidan är programmerad i angular och använder sig av ajax requests, därför krävs det att den körs i en servermiljö för att fungera.

# Installation Android #
Bland filerna finns även en .apk fil som går att installera som en mobilapp på smartphones med android.

Eftersom filen inte installeras via Google play krävs det att man tillåter installationer från okända källor. Via Inställningar > Säkerhet > och checkar rutan under "okända källor".
Sen är det bara att öppna filen via valfri filhanterare, dropbox eller liknande.

**obs**: Registrering och följa filmer finns ännu inte på android, men det kommer.

# Länk #
http://philiprobinson.se/movie/